namespace TcpSocketShowcase.TcpLayer.Messages
{
    public class WaterMessage : DeviceMessage
    {
        public int WaterLevel { get; set; }
    }
}