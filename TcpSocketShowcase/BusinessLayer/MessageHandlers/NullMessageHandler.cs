using TcpSocketShowcase.TcpLayer.Infrastructure;
using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase.BusinessLayer.MessageHandlers
{
    public class NullMessageHandler : IDeviceMessageHandler<NullDeviceMessage>
    {
        public void Handle(NullDeviceMessage message)
        {
            // Here you can handle message types that are not handler.
            // For example you can save messages anyway (for future processing)
        }
    }
}