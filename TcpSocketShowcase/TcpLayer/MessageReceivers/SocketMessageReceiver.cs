using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TcpSocketShowcase.TcpLayer.Interfaces;

namespace TcpSocketShowcase.TcpLayer.MessageReceivers
{
    public class SocketMessageReceiver : ISocketMessageReceiver
    {
        private readonly ISocketMessageParsingStrategyFactory _messageParsingStrategyFactory;

        private readonly IDeviceMessageDispatcher _messageDispatcher;

        private const int MaximumBufferSize = 1024;

        public SocketMessageReceiver(
            ISocketMessageParsingStrategyFactory messageParsingStrategyFactory,
            IDeviceMessageDispatcher messageDispatcher)
        {
            _messageParsingStrategyFactory = messageParsingStrategyFactory;
            _messageDispatcher = messageDispatcher;
        }

        public async Task StartReceivingAsync(TcpClient tcpClient, CancellationToken token)
        {
            var networkStream = tcpClient.GetStream();

            while (tcpClient.Connected && !token.IsCancellationRequested)
            {
                var buffer = new byte[MaximumBufferSize];

                await networkStream.ReadAsync(buffer, 0, buffer.Length, token);

                var message = Encoding.UTF8.GetString(buffer).Trim(' ');

                var parsingStrategy = _messageParsingStrategyFactory.GetParsingStrategy(message);

                var parsedMessage = parsingStrategy.Parse(message);

                await Task.Factory.StartNew(() =>_messageDispatcher.Dispatch(parsedMessage));
            }
        }
    }
}