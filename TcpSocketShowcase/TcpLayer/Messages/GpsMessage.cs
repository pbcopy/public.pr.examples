namespace TcpSocketShowcase.TcpLayer.Messages
{
    public class GpsMessage : DeviceMessage
    {
        public double Longitude { get; set; }

        public double Latitude { get; set; }
    }
}