using System.Linq;
using TcpSocketShowcase.TcpLayer.Interfaces;
using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase.TcpLayer.Parsing
{
    public class WaterSocketMessageParsingStrategy : ISocketMessageParsingStrategy
    {
        // Example: W,5
        public DeviceMessage Parse(string message)
        {
            var values = message.Split(',');
            var waterLevelString = values.Skip(1).First();

            var waterLevel = int.Parse(waterLevelString);

            return new WaterMessage
            {
                WaterLevel = waterLevel,
            };
        }
    }
}