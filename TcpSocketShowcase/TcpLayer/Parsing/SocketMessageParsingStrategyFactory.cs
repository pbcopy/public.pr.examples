using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using TcpSocketShowcase.TcpLayer.Interfaces;

namespace TcpSocketShowcase.TcpLayer.Parsing
{
    public class SocketMessageParsingStrategyFactory : ISocketMessageParsingStrategyFactory
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public SocketMessageParsingStrategyFactory(
            IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        public ISocketMessageParsingStrategy GetParsingStrategy(string message)
        {
            var firstCharacter = message.First();

            var scope = _serviceScopeFactory.CreateScope();

            if (firstCharacter == 'W')
                return scope.ServiceProvider.GetRequiredService<WaterSocketMessageParsingStrategy>();

            if (firstCharacter == 'G')
                return scope.ServiceProvider.GetRequiredService<GpsSocketMessageParsingStrategy>();

            return scope.ServiceProvider.GetRequiredService<NullMessageParsingStrategy>();
        }
    }
}