using System.Linq;
using TcpSocketShowcase.TcpLayer.Interfaces;
using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase.TcpLayer.Parsing
{
    public class GpsSocketMessageParsingStrategy : ISocketMessageParsingStrategy
    {
        // Example: G,0.1,0.1
        public DeviceMessage Parse(string message)
        {
            var values = message.Split(',');

            var longitudeString = values.Skip(1).First();
            var longitude = double.Parse(longitudeString);

            var latitudeString = values.Skip(2).First();
            var latitude = double.Parse(latitudeString);

            return new GpsMessage
            {
                Latitude = latitude,
                Longitude = longitude,
            };
        }
    }
}