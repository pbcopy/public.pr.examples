using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace TcpSocketShowcase.TcpLayer.Interfaces
{
    public interface ISocketMessageReceiver
    {
        Task StartReceivingAsync(TcpClient tcpClient, CancellationToken token);
    }
}