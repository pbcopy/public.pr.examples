using TcpSocketShowcase.TcpLayer.Infrastructure;
using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase.BusinessLayer.MessageHandlers
{
    public class WaterMessageHandler : IDeviceMessageHandler<WaterMessage>
    {
        public void Handle(WaterMessage message)
        {
            // Here you can handle WaterMessage
        }
    }
}