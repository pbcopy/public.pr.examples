namespace TcpSocketShowcase.TcpLayer.Interfaces
{
    public interface ISocketMessageParsingStrategyFactory
    {
        ISocketMessageParsingStrategy GetParsingStrategy(string message);
    }
}