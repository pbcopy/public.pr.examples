using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase.TcpLayer.Interfaces
{
    public interface ISocketMessageParsingStrategy
    {
        DeviceMessage Parse(string message);
    }
}