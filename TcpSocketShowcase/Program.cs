﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using TcpSocketShowcase.TcpLayer.Infrastructure;
using TcpSocketShowcase.TcpLayer.Interfaces;
using TcpSocketShowcase.TcpLayer.MessageReceivers;
using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddTransient<IDeviceMessageDispatcher, DeviceMessageDispatcher>()
                .AddTransient<ISocketMessageReceiver, SocketMessageReceiver>();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}