using System;

namespace TcpSocketShowcase.TcpLayer.Messages
{
    public abstract class DeviceMessage
    {
        public readonly Guid MessageId;

        protected DeviceMessage()
        {
            MessageId = Guid.NewGuid();
        }
    }
}