using TcpSocketShowcase.TcpLayer.Infrastructure;
using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase.BusinessLayer.MessageHandlers
{
    public class GpsMessageHandler : IDeviceMessageHandler<GpsMessage>
    {
        public void Handle(GpsMessage message)
        {
            // Here you can handle GpsMessage
        }
    }
}