using TcpSocketShowcase.TcpLayer.Interfaces;
using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase.TcpLayer.Parsing
{
    public class NullMessageParsingStrategy : ISocketMessageParsingStrategy
    {
        public DeviceMessage Parse(string message)
        {
            return new NullDeviceMessage();
        }
    }
}