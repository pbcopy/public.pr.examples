using System;
using Microsoft.Extensions.DependencyInjection;
using TcpSocketShowcase.TcpLayer.Interfaces;
using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase.TcpLayer.Infrastructure
{
    public class DeviceMessageDispatcher : IDeviceMessageDispatcher
    {
        private readonly IServiceProvider _serviceProvider;

        public DeviceMessageDispatcher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void Dispatch<TMessage>(TMessage message)
            where TMessage : DeviceMessage
        {
            var handler = _serviceProvider.GetRequiredService<IDeviceMessageHandler<TMessage>>();
            handler.Handle(message);
        }
    }

    public interface IDeviceMessageHandler<in T>
        where T : DeviceMessage
    {
        void Handle(T message);
    }
}