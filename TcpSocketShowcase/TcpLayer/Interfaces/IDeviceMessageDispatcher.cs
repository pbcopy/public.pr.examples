using TcpSocketShowcase.TcpLayer.Messages;

namespace TcpSocketShowcase.TcpLayer.Interfaces
{
    public interface IDeviceMessageDispatcher
    {
        void Dispatch<TMessage>(TMessage message)
            where TMessage : DeviceMessage;
    }
}